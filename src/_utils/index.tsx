import {ITodo, IUser, IUserInformation} from "../_interfaces";

export const fetchUserById: (id: number) => Promise<IUser> = (id: number) => {
    return fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
        .then(res => res.json())
}

export const fetchTodosByUserId: (id: number) => Promise<ITodo[]> = (id: number) => {
    return fetch(`https://jsonplaceholder.typicode.com/todos?userId=${id}`)
        .then(res => res.json())
}

export const fetchUsers: () => Promise<IUser[]> = () => {
    return fetch('https://jsonplaceholder.typicode.com/users')
        .then(res => res.json())
}

export const fetchUserInformation: (id: number) => Promise<Partial<IUserInformation>> = (id: number) => {
    let model: Partial<IUserInformation> = {};

    return fetchUserById(id)
        .then((user: IUser) => {
            model.user = user;
            return fetchTodosByUserId(model.user.id);
        })
        .then((todos: ITodo[]) => {
            model.todos = todos;
            return model;
        })
}


export const updateTodoState: (todo: ITodo) => Promise<{id: number}> = (todo: ITodo) => {
    const options = {
        method: 'POST',
        body: JSON.stringify({...todo, completed: true})
    };
    const url = `https://jsonplaceholder.typicode.com/todos?userId=${todo.id}`;

    return fetch(url, options).then(res => res.json())
}

