import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {
    HashRouter as Router,
    Routes,
    Route,
} from 'react-router-dom';

import reportWebVitals from './reportWebVitals';

import UserComponent from "./User";
import App from "./App";
import NavigationComponent from "./Navigation";
import UsersComponent from "./Users/index";


ReactDOM.render(
    <React.StrictMode>
        <main className="Main">
            <div className="Main__container">
                <Router>
                    <NavigationComponent/>

                    <Routes>
                        <Route path="users" element={<UsersComponent />}>
                            <Route path=":id" element={<UserComponent/>}/>
                        </Route>

                        <Route path="/" element={<App/>}/>
                    </Routes>
                </Router>

            </div>
        </main>
    </React.StrictMode>,
    document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
