import {useEffect, useState} from "react";
import {fetchTodosByUserId, fetchUserById, fetchUsers} from "../_utils";
import {ITodo, IUser} from "../_interfaces";

export const useTodosByUserId = (id: number) => {
    const [todos, setTodos] = useState<ITodo[]>([]);

    useEffect(() => {
        fetchTodosByUserId(id)
            .then(setTodos)
            .catch(console.error);
    }, [id]);

    return todos;
}

export const useUsers = () => {
    const [users, setUsers] = useState<IUser[]>([]);

    useEffect(() => {
        fetchUsers()
            .then(setUsers)
            .catch(console.error);
    }, []);

    return users;
}

export const useUserById = (id: number) => {
    const [user, setUser] = useState<IUser | null>(null);

    useEffect(() => {
        fetchUserById(id)
            .then(setUser)
            .catch(console.error);
    }, [id]);

    return user;
}
