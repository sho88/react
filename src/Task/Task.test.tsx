import {cleanup, fireEvent, render, screen} from '@testing-library/react';
import Task from './index'

describe('Task', () => {

    afterAll(() => {
        cleanup();
    })

    test('do not render the "Task" component without "data" prop', () => {
        render(<Task/>)
        expect(screen.queryByTestId(`task-1`)).toBeNull();
    });

    test('render the "Task" component with its corresponding data', () => {
        const props = {
            data: {
                id: 1,
                title: 'Write Tests First...',
                completed: false,
                userId: 1,
            }
        };
        render(<Task {...props} />);
        expect(screen.getByText(props.data.title)).toBeInTheDocument();
    });

    test('emit the handleToggleItem method', () => {
        const props = {
            data: {
                id: 1,
                title: 'Write Tests First...',
                completed: false,
                userId: 1,
            },
            handleToggleItem: () => {}
        }

        jest.spyOn(props, 'handleToggleItem');
        render(<Task {...props} />);

        fireEvent.click(screen.getByTestId(`task-${props.data.id}`));
        expect(props.handleToggleItem).toHaveBeenCalledWith(props.data);
    });
});
