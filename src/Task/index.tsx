import React, {Fragment} from "react";
import "./Task.css";
import {ITodo} from "../_interfaces";

export interface ITasksListProps {
    data: ITodo;
    handleToggleItem: (todoItem: ITodo) => void;
}

export default React.memo((props: Partial<ITasksListProps>) => {
    let className: string = 'Task--completed';

    // events go here...
    const handleClick = () => props.handleToggleItem && props.data && props.handleToggleItem(props.data);

    return (
        <Fragment>
            {props.data && (
            <div className={`Task ${props.data?.completed && className}`}
                 data-testid={`task-${props.data?.id}`}
                 key={props.data?.id}
                 onClick={handleClick}>
                {props.data?.title}
            </div>)}
        </Fragment>
    )
});
