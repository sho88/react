import React, {useContext, useEffect, useState} from "react";
import {useParams} from 'react-router-dom';

import {fetchUserInformation, updateTodoState} from "../_utils";
import {ITodo, IUser, IUserInformation} from "../_interfaces";
import {Context} from "../_context";
import Task from "../Task";

export default function UserComponent() {
    const params = useParams();
    const [loaded, setLoaded] = useState<boolean>(false);
    const [todos, setTodos] = useState<ITodo[] | undefined>(undefined);
    const [user, setUser] = useState<IUser | undefined>(undefined);
    const {dispatch} = useContext(Context);

    useEffect(() => {
        if (!params.id) {
            setLoaded(false);
            return;
        }

        fetchUserInformation(+params.id)
            .then(({todos, user}: Partial<IUserInformation>) => {
                setUser(user);
                setTodos(todos);
                dispatch({type: '[SELECT_USER]', user});
            })
            .catch(console.error) // @TODO: Sort this out
            .finally(() => setLoaded(true));
    }, [params.id])

    // events go here...
    const handleToggleItem = (todoItem: ITodo) => {
        if (todoItem.completed) return;

        updateTodoState(todoItem)
            .then(() => (
                setTodos(todos?.map((todo: ITodo) => ({
                    ...todo,
                    completed: todoItem.id !== todo.id ? todo.completed : true
                })))
            ))
            .catch(console.error) // @TODO: Come back to handle this...
    }

    return (
        <div>
            {!loaded && 'Loading...'}
            {loaded && user && (
                <div>
                    <pre>Tasks for {user.name} ({user.email})</pre>
                    <hr/>
                    {todos?.map(todo => (
                        <Task key={todo.id} data={todo} handleToggleItem={handleToggleItem}/>
                    ))}
                </div>
            )}
        </div>
    )
}

