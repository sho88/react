import './Users.css';
import {useTodosByUserId, useUserById, useUsers} from "../_hooks";
import {Link, Outlet} from "react-router-dom";
import React, {useReducer} from "react";
import {Context, initialState, IState} from "../_context";
import {IUser} from "../_interfaces";

const appReducer = (state: IState, action: Partial<any>) => {
    switch (action.type) {
        case '[SELECT_USER]':
            return {...state, selected: action.user}
        case '[DESELECT_USER]':
            return {...state, selected: null}
        default:
            return state;
    }
}

export default function UsersComponent() {
    const userId = 1;
    const users = useUsers();
    const todos = useTodosByUserId(userId);
    const user = useUserById(userId);
    const [state, dispatch] = useReducer(appReducer, initialState);

    return (
        <Context.Provider value={{dispatch, state}}>
            <div className="App-parent">
                <div className="users">
                    {users.map((user: IUser) => (
                        <p key={user.id}>
                            <Link to={`/users/${user.id}`}>{user.name}</Link>
                        </p>
                    ))}
                </div>

                <div>
                    {!state.selected && <pre>{JSON.stringify(user, null, 2)}</pre>}
                    {!state.selected && <pre>{JSON.stringify(todos, null, 2)}</pre>}
                    <Outlet/>
                </div>
            </div>
        </Context.Provider>
    );
};
