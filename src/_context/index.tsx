import {IUser} from "../_interfaces";
import {createContext} from "react";

// interfaces here...
export interface IState {
    selected: IUser | null;
}

// constants here...
export const initialState: IState = {
    selected: null,
}

export const Context = createContext<any>({
    state: initialState,
    dispatch: null
});

