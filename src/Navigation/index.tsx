import {Link} from "react-router-dom";

import './Navigation.css';

export default function NavigationComponent() {

    return (
        <nav className="Navigation">
            <Link to="/">Home</Link>
            <Link to="/users">Users</Link>
        </nav>
    );
}
